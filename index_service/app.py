from flask import Flask, request, jsonify
import numpy as np
import joblib
import json
import os

# CLUSTER_ID = '0'
CLUSTER_ID = os.environ['CLUSTER_ID']
index = joblib.load(f'/indices/clust_{CLUSTER_ID}/index')
with open(f'/indices/clust_{CLUSTER_ID}/sentences.json') as f:
    raw_sentences = json.load(f)
app = Flask(__name__)

class Model:
    def __init__(self):
        pass
    def predict(self, x):
        return np.random.randint(0, len(x))


@app.route('/get_best_from_k', methods=['POST', 'GET'])
def get_neighbours():
    data = request.json
    input_emb = np.array(data['emb'], dtype='float32')
    dists, neigh_ind = index.search(input_emb.reshape(1, -1), 30)
    neigh_ind = neigh_ind.flatten()
    neigh_emb = np.array([index.reconstruct(int(ind)) for 
                    ind in neigh_ind], dtype='float32')
    argmax_closest_emb = predict_match(neigh_emb)
    best_match_ind = neigh_ind[argmax_closest_emb]
    best_match_sentence = raw_sentences[best_match_ind]
    return jsonify(best_match=best_match_sentence, cluster_id=CLUSTER_ID)

def predict_match(neigh_emb):
    mdl = Model()
    argmax_closest_emb = mdl.predict(neigh_emb)
    return argmax_closest_emb