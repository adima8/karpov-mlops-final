#!/bin/bash
# docker service create --replicas 1 --name web_app --publish published=10001,target=10001 49.12.186.172:5000/web_app 
docker run --rm -it -v /opt/indices/:/indices -p 5001:5001 --add-host=host.docker.internal:host-gateway -e CLUSTER_ID=1  index_service
docker run --rm -it -p 5005:5005 --add-host=host.docker.internal:host-gateway  gateway
docker run -d --rm -p 8501:8501 -v "/opt/models:/models" -e MODEL_NAME="use_multi" tensorflow/serving

docker service create --replicas 1 --host host.docker.internal:host-gateway --name gateway --mount type=bind,src=/opt/indices/,dst=/indices -p 5005:5005 49.12.186.172:4999/gateway

docker service create --replicas 1 --name index_service_0 -p 5000:5001  --mount type=bind,src=/opt/indices/,dst=/indices -e CLUSTER_ID=0 49.12.186.172:4999/index_service

docker service create --replicas 1 --name index_service_1 -p 5001:5001 --mount type=bind,src=/opt/indices/,dst=/indices -e CLUSTER_ID=1 49.12.186.172:4999/index_service

docker service create --replicas 1 --name index_service_2 -p 5002:5001 --mount type=bind,src=/opt/indices/,dst=/indices -e CLUSTER_ID=2 49.12.186.172:4999/index_service

docker service create --replicas 1 --name index_service_3 -p 5003:5001 --mount type=bind,src=/opt/indices/,dst=/indices --constraint node.hostname==kcloud-production-user-107-vm-306 -e CLUSTER_ID=3 49.12.186.172:4999/index_service


docker service create --replicas 1 --name serving -p 8501:8501 --mount type=bind,src=/opt/models,dst=/models -e MODEL_NAME="use_multi" tensorflow/serving


curl http://49.12.186.172:4999/v2/_catalog