docker run -d -p 4999:5000 --restart=always --name registry registry:2
# docker start registry

docker tag gateway 49.12.186.172:4999/base
docker push 49.12.186.172:4999/base

docker tag gateway 49.12.186.172:4999/gateway
docker push 49.12.186.172:4999/gateway

docker tag index_service 49.12.186.172:4999/index_service
docker push 49.12.186.172:4999/index_service